/* 
Buat fungsi untuk menghitung luas dan keliling lingkaran
*/

function menghitungLingkaran (r) {
    const pi = 3.14;
    const luas = 2 * pi * r;
    const Keliling = pi * r * r;
    return {keliling, luas};
};

const {keliling, luas} = menghitungLingkaran(4)
console.log(keliling);
console.log(luas);
