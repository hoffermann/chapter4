// #1 Gunakan metode untuk membalikan isi array

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];

//ANSWER
const reversed = fruits.reverse();
let reversedList = " ";
for (let x in reversed) {
  reversedList += reversed[x] + "\n"; 
}

console.log(reversedList)

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

let month = [
  "January",
  "February",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

//ANSWER
month.splice(2, 0, "March", "April", "May", "June");
month.forEach(function(month) {
    console.log(month);
}
)


/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3 Mengambil beberapa kata terakhir dalam string
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

//ANSWER
console.log(message1Array[3], message1Array[4], message1Array[5], message1Array[6])
/* 
Output: belajar Javascript sangat menyenangkan
*/

// #4 Buat agar awal kalimat jadi kapital
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

//ANSWER
var message2Array = message2.split(' ');
console.log(message2Array[3].charAt(0).toUpperCase() + message2Array[3].slice(1), message2Array[4], message2Array[5], message2Array[6])

/* 
Output: Belajar Javascript sangat menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall

Output:
BMI Steven =
BMI Bill = 
*/

function countBMI(W, H) 
{
    return BMI = W / (H**2)
}

console.log("BMI Steven " + countBMI(78, 1.69))
console.log("BMI Bill " + countBMI(92, 1.95))


/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/

let johnW = 95;    
let johnH = 1.88;
let nashW = 85;
let nashH = 1.76;

let johnBMI = BMI(johnW, johnH);
let nashBMI = BMI(nashW, nashH);

if(johnBMI > nashBMI) {
    console.log(`John's BMI (${johnBMI}) is higher than Nash's (${nashBMI})`)
}
else if(johnBMI < nashBMI) {
    console.log(`John's BMI (${johnBMI}) is lower than Nash's (${nashBMI})`);
}


//  #7 Looping

let data = [10, 20, 30, 40, 50];

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/
let total = 0;

for (let x in data) {
    total += data[x];
}
console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
